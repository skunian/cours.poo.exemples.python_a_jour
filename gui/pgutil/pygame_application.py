# -*- coding: utf-8 -*-

"""
Ce module propose une classe pour simolifier l'usage de PyGame.
"""

import pygame as pg
import pygame.display as dsp


class PygameApplication(object):
    """Cette classe représente une application Pygame."""

    DEFAULT_TITLE = "Pygame Application"
    RESOLUTION = (640, 480)
    UPDATE_RATE = 30

    def __init__(self,
                 title=DEFAULT_TITLE,
                 resolution=RESOLUTION,
                 update_rate=UPDATE_RATE):
        """Initialise l'application.
        :param str title: Nom de la fenêtre
        :param tuple resolution: taille de la fenêtre
        :param int update_rate: taux de rafraichissement de la vue
        """
        self.__title = title
        self.__resolution = resolution
        self.__update_rate = update_rate
        self.__surface = None
        self.__clock = None
        self.__scene = None

    def run(self, scene):
        """Exécute l'application."""
        self.pg_init(scene)
        self.pg_validate()

        end = False
        while not end:
            end = self.pg_handle_event()
            self.pg_update()
            self.pg_draw()
        self.pg_quit()

    def pg_init(self, scene):
        """Initialise le système graphique."""
        pg.init()
        self.__surface = dsp.set_mode(self.__resolution, pg.DOUBLEBUF)
        dsp.set_caption(self.__title)
        self.__clock = pg.time.Clock()
        self.__scene = scene
        self.__scene.on_enter()

    def pg_validate(self):
        """Vérifie que l'application peut s'exécuter."""
        if self.__scene is None:
            raise ValueError("Aucune scène n'est définie")
        if self.__surface is None:
            raise ValueError("Aucune surface n'est définie")
        if self.__clock is None:
            raise ValueError("Aucune timer n'est définie")

    def pg_handle_event(self):
        """Gère les évènements."""
        for event in pg.event.get():
            self.__scene.on_event(event)
            return event.type == pg.QUIT

    def pg_update(self):
        """Met à jour l'affichage."""
        deltat = self.__clock.tick(self.__update_rate)
        self.__scene.update(deltat)

    def pg_draw(self):
        """Dessine la scéne."""
        self.__scene.draw(self.__surface)
        pg.display.flip()

    def pg_quit(self):
        """Termine l'application."""
        self.__scene.on_exit()
        pg.quit()
