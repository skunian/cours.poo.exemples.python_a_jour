# -*- coding: utf-8 -*-

"""
Ce module propose une classe pour représenter une scène avec Pygame.
"""

from abc import ABC, abstractmethod


class PygameScene(ABC):
    """Cette classe abstraite représente une scène Pygame."""

    @abstractmethod
    def draw(self, surface):
        """Dessine la scène."""
        pass

    def update(self, deltat):
        """Met à jour la scène."""
        pass

    def on_event(self, event):
        """Gère l'évènement Pygame.
        :param pygame.event.Event event: évènement à traiter
        """
        pass

    def on_enter(self):
        """Initialise la scène."""
        pass

    def on_exit(self):
        """Appelé lors de la fin de l'application."""
        pass
