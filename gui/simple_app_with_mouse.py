#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Ce module est une application graphique pygame simple."""

import pygame as pg
from pygame import Color
import pygame.draw as pgdr

from pgutil.pygame_application import PygameApplication
from pgutil.pygame_scene import PygameScene

# Modifiez la classe ci-dessous selon l'énoncé de l'exercice 2
class SimpleCircle(PygameScene):
    def draw(self, surface):
        """Dessine la scène."""
        pgdr.circle(surface, Color("red"), surface.get_rect().center, 50)

class SimpleCircleSolution(PygameScene):
    def __init__(self):
        self.position = None

    def draw(self, surface):
        """Dessine la scène."""
        if self.position is not None:
            surface.fill(Color("black"))
            pgdr.circle(surface, Color("red"), self.position, 50)

    def on_event(self, event):
        """Gère l'évènement Pygame.
        :param pygame.event.Event event: évènement à traiter
        """
        if event.type == pg.MOUSEBUTTONUP:
            self.position = event.pos

if __name__ == '__main__':
    app = PygameApplication()
    app.run(SimpleCircle())
